require 'rspec/autorun'

Dir['spec/*_spec.rb'].each { |f|
  if f = "spec/merb_helpers_form_spec.rb"
    next
  end
  require f
}
